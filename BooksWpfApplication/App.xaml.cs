﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Data;
using System.Xml;
using System.Configuration;
using FilmsWpfApplication.Views;

namespace FilmsWpfApplication
{
    public partial class App : Application
    {
        public List<Film> Films;
        void AppStartup(object sender, StartupEventArgs args)
        {            
            Films = new List<Film>();
            Film lotr = new Film("The Lord of the Rings", "Peter Jackson", 2001, Genre.Fantasy, 178);
            Film harryPotter = new Film("Harry Potter", "Chris Columbus", 2001, Genre.Fantasy, 152);
            Film forrestGump = new Film("Forrest Gump", "Robert Zemeckis", 1994, Genre.Drama, 142);
            Film schindlersList = new Film("Schindler's List", "Steven Spielberg", 1993, Genre.History, 195);
            Film backToTheFuture = new Film("Back to the Future", "Robert Zemeckis", 1985, Genre.Comedy, 116);
            Film pirates = new Film("Pirates of the Caribbean", "Gore Verbinski", 2003, Genre.Fantasy, 143);
            Film titanic = new Film("Titanic", "James Cameron", 1997, Genre.Drama, 194);
            Film sherlock = new Film("Sherlock", "Paul McGuigan", 2010, Genre.Detective, 90);

            Films.Add(lotr);
            Films.Add(harryPotter);
            Films.Add(forrestGump);
            Films.Add(schindlersList);
            Films.Add(backToTheFuture);
            Films.Add(pirates);
            Films.Add(titanic);
            Films.Add(sherlock);
            MainWindow mainWindow = new MainWindow();
            MainViewModel mainViewModel = new MainViewModel(Films);
            mainWindow.DataContext = mainViewModel;
            mainWindow.Show();
        }
    }
}