﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FilmsWpfApplication
{
    [ValueConversion(typeof(int), typeof(String))]
    public class TimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int time = (int)value;
            int hours = time / 60;
            int minutes = time % 60;
            string result = hours + " hours " + minutes + " minutes";
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string time = value as string;
            string[] tmp = time.Split(' ');
            int hours, minutes;
            if (tmp.Length > 2 && Int32.TryParse(tmp[0], out hours) && Int32.TryParse(tmp[2], out minutes))
            {
                int timeInt = hours * 60 + minutes;
                return timeInt;
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
