﻿using FilmWpfApplication.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FilmsWpfApplication
{
    public class FilmViewModel : ViewModelBase
    {
        private Film film;
        public Window view;
        public ObservableCollection<FilmViewModel> films;
        public int index;

        public string Name
        {
            get { return film.Name; }
            set { film.Name = value; OnPropertyChanged("Name"); }
        }
        public State State
        {
            get { return film.State; }
            set { film.State = value; OnPropertyChanged("State"); }
        }
        public string Director
        {
            get { return film.Director; }
            set { film.Director = value; OnPropertyChanged("Director"); }
        }
        public int Year
        {
            get { return film.Year; }
            set { film.Year = value; OnPropertyChanged("Year"); }
        }
        public Genre Genre
        {
            get { return film.Genre; }
            set { film.Genre = value; OnPropertyChanged("Genre"); }
        }
        public int Duration
        {
            get { return film.Duration; }
            set { film.Duration = value; OnPropertyChanged("Duration"); }
        }

        public FilmViewModel(Film aFilm)
        {
            film = aFilm;
        }

        public FilmViewModel(Film aFilm, Window aWindow, ObservableCollection<FilmViewModel> m)
        {
            film = aFilm;
            view = aWindow;
            films = m;
        }

        public FilmViewModel(Film aFilm, Window aWindow, ObservableCollection<FilmViewModel> m, int index)
        {
            film = aFilm;
            view = aWindow;
            films = m;
            this.index = index;
        }

        public void Update(FilmViewModel newFilm)
        {
            film = new Film(newFilm.Name, newFilm.Director, newFilm.Year, newFilm.Genre, newFilm.Duration);
        }

        public ICommand SubmitCommand
        {
            get { return new DelegateCommand<FilmViewModel>(AddFilm, FuncToEvaluate); }
        }

        private void AddFilm(object context)
        {
            bool error = false;
            if (film == null)
                error = true;
            else
            {
                if (film.Name == null)
                    error = true;
                else
                {
                    error = false;
                    FilmViewModel f = (FilmViewModel)view.DataContext;
                    film = new Film(f.Name, f.Director, f.Year, f.Genre, f.Duration);
                    films.Add(f);
                }
            }
            if (!error)
                view.Close();
            else
                MessageBox.Show("You must fill all fields!");
        }

        private bool FuncToEvaluate(object context)
        {
            return true;
        }

        public ICommand EditCommand
        {
            get { return new DelegateCommand<FilmViewModel>(EditFilm, FuncToEvaluate); }
        }

        private void EditFilm(object context)
        {
            FilmViewModel newFilm = (FilmViewModel)view.DataContext;
            if (index != -1)
            {
                films[index] = newFilm;
                films[index].OnPropertyChanged("Year");
            }
            view.Close();
        }
    }
}
