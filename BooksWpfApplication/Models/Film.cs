﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilmsWpfApplication
{
    public enum Genre
    {
        Detective,
        Drama,
        History,
        Comedy,
        Thriller,
        Fantasy
    }

    public enum State
    {
        None,
        Less,
        More,
        Between
    }

    public class Film
    {
        public string Name { get; set; }
        public string Director { get; set; }
        public int Year { get; set; }
        public int Duration { get; set; }
        public Genre Genre { get; set; }

        public State State { get; set; }    

        public Film() { }

        public Film(string aName, string aDirector, int aYear, Genre aGenre, int aDuration)
        {
            Name = aName;
            Director = aDirector;
            Year = aYear;
            Genre = aGenre;
            Duration = aDuration;
        }

    }
}
