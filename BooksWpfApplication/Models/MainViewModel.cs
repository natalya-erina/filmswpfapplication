﻿using BooksWpfApplication.Commands;
using FilmsWpfApplication.Views;
using FilmWpfApplication.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace FilmsWpfApplication
{

    public class MainViewModel : ViewModelBase
    {
        public ObservableCollection<FilmViewModel> filmsList { get; set; }
        
        public MainViewModel(List<Film> films)
        {
            filmsList = new ObservableCollection<FilmViewModel>(films.Select(f => new FilmViewModel(f)));
            EventManager.RegisterClassHandler(typeof(ListBoxItem), ListBoxItem.MouseLeftButtonDownEvent, new RoutedEventHandler(OnMouseLeftButtonDown));
        }

        private void OnMouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            EditFilmWindow window = new EditFilmWindow();
            ListBoxItem item = sender as ListBoxItem;
            FilmViewModel f = (FilmViewModel)item.DataContext;
            int index = filmsList.IndexOf(f);
            f.view = window;
            f.films = filmsList;
            f.index = index;
            window.DataContext = f;
            window.Show();
        }

        private bool _GroupView;
        public bool GroupView
        {
            get
            {
                return _GroupView;
            }
            set
            {
                if (value != _GroupView)
                {
                    var view = CollectionViewSource.GetDefaultView(filmsList);                  
                    view.GroupDescriptions.Clear();
                    if (value)
                    {
                        view.GroupDescriptions.Add(new PropertyGroupDescription("Genre"));
                    }
                    _GroupView = value;
                    OnPropertyChanged("GroupView");
                }
            }
        }

        private bool _FilterDisabled;
        public bool FilterDisabled
        {
            get
            {
                return _FilterDisabled;
            }
            set
            {
                if (value != _FilterDisabled)
                {
                    var view = CollectionViewSource.GetDefaultView(filmsList);

                    if (value)
                    {
                        foreach(FilmViewModel fvm in filmsList)
                            fvm.State = State.None;
                        view.Refresh();
                    }
                    _FilterDisabled = value;
                    OnPropertyChanged("FilterDisabled");
                }
            }
        }

        public void UpdateFilm(FilmViewModel newFilm)
        {
            filmsList.FirstOrDefault(filmViewModel => filmViewModel.Name == newFilm.Name).Update(newFilm);
        }

        public ICommand MyButtonClickCommand
        {
            get { return new DelegateCommand<object>(AddFilm, CanAdd); }
        }

        private void AddFilm(object context)
        {
            AddFilmWindow window = new AddFilmWindow();
            FilmViewModel f = new FilmViewModel(new Film(), window, filmsList);
            window.DataContext = f;
            window.Show();
        }

        private bool CanAdd(object context)
        {
            return true;
        }

        public ICommand FilterCommand
        {
            get { return new RelayCommand((txt) => Filter(txt)); }
        }

        private void Filter(object context)
        {
            FilterDisabled = false;
            var values = (object[])context;
            if ((values[0] as string).Length == 0 && (values[1] as string).Length == 0)
            {
                foreach (FilmViewModel fvm in filmsList)
                    fvm.State = State.None;
                return;
            }
            int year1, year2;
            if (Int32.TryParse(values[0] as string, out year1) && 
                Int32.TryParse(values[1] as string, out year2))
            {
                foreach(FilmViewModel fvm in filmsList)
                {
                    if (fvm.Year >= year1 && fvm.Year <= year2)
                        fvm.State = State.Between;
                    else
                        if (fvm.Year < year1)
                        fvm.State = State.Less;
                    else
                            if (fvm.Year > year2)
                        fvm.State = State.More;
                }
            }
        }
    }
}
